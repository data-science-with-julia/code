using Debugger

function add(first_number, second_number)
    total = first_number + second_number
    @bp
    return total
end


function add(numbers)
    total = 0
    
    for number in numbers
        total += number
        @bp
    end
    
    total
end
    
        
println(add(3, 5))
