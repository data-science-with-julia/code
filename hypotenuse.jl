using Infiltrator

opposite = 3
adjcent = 4

sum_of_squares = (opposite * opposite) + (adjcent * adjcent)

hypotenuse = sqrt(sum_of_squares)

@infiltrate

println("Hypotenuse: ", hypotenuse)
